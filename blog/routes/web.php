<?php

use Illuminate\Support\Facades\Route;



Route::get('/', function () {
    return view('frontend.index');
});
Route::group([
	'namespace' => 'frontend'],
	function(){

		Route::get('/blog', 'BlogController@index')->name('blog.index');
		Route::get('/blog/single/{id}', 'BlogController@show')->name('blog.show');
		Route::get('/blog/single/{id}/post', 'BlogController@post')->name('blog.post');
		// Route::get('/blog/single/{id}/next', 'BlogController@next')->name('blog.next');
		// Route::get('/blog/single/{id}/previous', 'BlogController@previous')->name('blog.previous');

		Route::get('/member/login', 'UserController@login')->name('user.login');
		Route::post('/member/login', 'UserController@post_login')->name('user.post_login');

		Route::get('/member/register', 'UserController@register')->name('user.register');
		Route::post('/member/register', 'UserController@post_register')->name('user.post_register');
		Route::post('post/comment/{id}', 'BlogController@comment')->name('blog.comment');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
	'namespace' => 'Admin',
	'prefix' => 'admin'], 
	function(){
		Route::get('/home', 'DashboardController@index');

		Route::get('/profile', 'UserController@edit')->name('profile.edit');
		Route::post('/profile', 'UserController@update')->name('profile.update');

		Route::get('/country/list', 'CountryController@index')->name('country.index');
		Route::get('/country/add', 'CountryController@create')->name('country.create');
		Route::post('/country/add', 'CountryController@store')->name('country.store');
		Route::get('/country/delete/{id}', 'CountryController@destroy')->name('country.destroy');

		Route::get('/blog/list', 'BlogController@index')->name('blog.index');
		Route::get('/blog/add', 'BlogController@create')->name('blog.create');
		Route::post('/blog/add', 'BlogController@store')->name('blog.store');
		Route::get('/blog/{id}/edit', 'BlogController@edit')->name('blog.edit');
		Route::post('/blog/{id}/edit', 'BlogController@update')->name('blog.update'); 
		Route::get('/blog/{id}/delete', 'BlogController@destroy')->name('blog.destroy');

});