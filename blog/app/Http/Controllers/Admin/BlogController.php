<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\BlogRequest;
use App\Blog;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $DataBlog = Blog::paginate(3);
        return View('admin.blog.list',compact('DataBlog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.blog.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        $DataBlog = $request->all();
        $file = $request->image;
        if(!empty($file)){
            $DataBlog['image'] = $file->getClientOriginalName();
        }
       
        if(Blog::create($DataBlog)){
            if(!empty($file)){
                $file->move('admin/assets/images/users', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success', 'success');
        } else {
            return redirect()->back()->with('error', 'Fail');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataBlog = Blog::find($id);
        return View('admin.blog.edit',compact('dataBlog'));
     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogRequest $request, $id)
    {
        $data = $request->all();
        $Blog = Blog::findOrFail($id);

        $file = $request->image;
        if(!empty($file)){
            $data['image'] = $file->getClientOriginalName();
        }

        if(empty($data['title'])){
            $data['title'] = $Blog->title; 
        }else{
            $data['title'] = $request->title;
        }

         if(empty($data['description'])){
            $data['description'] = $Blog->description; 
        }else{
            $data['description'] = $request->description;
        }

        if(empty($data['content'])){
            $data['content'] = $Blog->content; 
        }else{
            $data['content'] = $request->content;
        }

        if($Blog->update($data)){
            if(!empty($file)){
                $file->move('admin/assets/images/users', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success', 'success');
        } else {
            return redirect()->back()->with('error', 'Fail');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Blog::find($id)->delete();
        return redirect()->back()->with('success','Delete susscess');
    }
}
