<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use App\Country;
use Move;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
        // $data = User::findOrFail($id);
        // return View('admin.user.profile', compact('data'));
        $data = auth::user();
        $countries = Country::all();
        return View('admin.user.profile', compact('data', 'countries'));
    }
    
    public function update(UserRequest $request)
    {
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        // $user = Auth::user();

        $data = $request->all();

         $file = $request->avatar;
        if(!empty($file)){
            $data['avatar'] = $file->getClientOriginalName();
        }

        if(empty($data['name'])){
            $data['name'] = $user->name;
        }else{
             $data['name'] = $data['name'];
        }

        if(empty($data['password'])){
            $data['password'] = $user->password;
        }else{
             $data['password'] = Hash::make($data['password']);
        }

        if(empty($data['phone'])){
            $data['phone'] = $user->phone;
        }else{
             $data['phone'] = $data['phone'];
        }

        if(empty($data['address'])){
            $data['address'] = $user->address;
        }else{
             $data['address'] = $data['address'];
        }

        if ($user->update($data)) { 
            if(!empty($file)){
                $file->move('admin/assets/images/users', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success','Update profile success.');
        } else {
            return redirect()->back()->withErrors('Update profile error.');
        }
    }
}
                                
                                
                                
                                
                                
                                
                                
                                
                                
          
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                                        
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                                 
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                