<?php

namespace App\Http\Controllers\frontend;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use Move;
class UserController extends Controller
{

  	public function login (){
  		return View('frontend.user.login');
  	}
  	public function post_login (UserRequest $request){
  		$login = [
  			'email' => $request->email,
  			'password' => $request->password,
  			'level' => 0
  		];
  		if(Auth::attempt($login)){
  			if(Auth::check()){
          return View('welcome');
        }
  		}else{
  			return riderect()->back();
  		}
  	}  

  	public function register (){
  		return View('frontend.user.register');
  	}
  	public function post_register (UserRequest $request){
  		$register = create([
  			'name' => $request->name,
  			'email' => $request->email,
  			'password' => Hash::make($request->password),
  			'phone' => $request->phone,
  			'address' => $request->address
  		]);
  		return riderect()->route('user.login');
  	} 

    
}
