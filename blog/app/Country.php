<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'country';

    public $timestamps = false;
    
    protected $fillable = ['country_name'];

    public function users(){
    	return $this->hasMany('App\User');
    }
}
