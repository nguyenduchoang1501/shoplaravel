<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table = 'rate';

    public $timestamps = false;
    
    protected $fillable = ['blog_id,rating'];

    public function blog(){
    	return $this->hasMany('App\Blog');
    }
}
