@extends('admin.layouts.master')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card card-body">
            <h4 class="card-title">Update Blog</h4>
            <h5 class="card-subtitle"> All bootstrap element classies </h5>
            <form action="{{route('blog.update',[$dataBlog['id']])}}" enctype="multipart/form-data" class="form-horizontal m-t-30" method="post">
                @csrf
                <div class="form-group">
                    <label>Title <span class="help">(*)</span></label>
                    <input type="text"  class="form-control" name="title" value="{{$dataBlog['title']}}">
                </div>
                 <div class="form-group">
                    <label>Image</label>
                    <input type="file" class="form-control" name="image" value="{{$dataBlog['image']}}">
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" rows="5" name="description" value="{{$dataBlog['description']}}"></textarea>
                </div>
                <div class="form-group">
                    <label>Content</label>
                    <textarea class="form-control" id="content" name="content"  rows="5"></textarea>
                </div>
                <div class="form-group">
	                <button type="submit" class="btn btn-success">Add</button>
            	</div>
            </form>
        </div>
    </div>
</div>
@endsection
