@extends('admin.layouts.master')
@section('content')
 <div class="container-fluid">
<div class="row">
    <div class="col-12">
    	@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('success'))
          <div class="alert alert-info">{{session('success')}}</div>
        @endif
        @if (session('error'))
          <div class="alert alert-danger">{{session('error')}}</div>
        @endif
        <div class="card card-body">
            <h4 class="card-title">List Country</h4>
            <h5 class="card-subtitle"> All Countries On World  </h5>
            <form action="{{route('country.create')}}" class="form-horizontal m-t-30" method="post">
            	@csrf 
                <div class="form-group">
                    <label>Country Name</label>
                    <input type="text" class="form-control" name="country_name">
                </div>
                <div class="form-group">
	                <button type="submit" class="btn btn-success">Add</button>
            	</div>
           </form>
       </div>
   </div>
</div>
@endsection