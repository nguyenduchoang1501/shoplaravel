@extends('admin.layouts.master')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Default Table</h4>
                <h6 class="card-subtitle">Using the most basic table markup, here’s how <code>.table</code>-based tables look in Bootstrap. All table styles are inherited in Bootstrap 4, meaning any nested tables will be styled in the same manner as the parent.</h6>
                <h6 class="card-title m-t-40"><i class="m-r-5 font-18 mdi mdi-numeric-1-box-multiple-outline"></i> Table With Outside Padding</h6>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Country Name</th>
                                <th scope="col">Action</th>
                                <th scope="col"><a href="{{route('country.create')}}" >Add</a></th>
                            </tr>
                        </thead>
                        <?php foreach($data as $values){?>
                        <tbody>
                            <tr>
                                <th scope="row">{{$values['id']}}</th>
                                <td>{{$values['country_name']}}</td>
                                <td><a href="{{route('country.destroy',$values['id'])}}" >Delete/</a></td>
                            </tr>
                        </tbody>
                        <?php }?>
                    </table>
                    {{$data->links()}}
                </div>

                <h6 class="card-title"><i class="m-r-5 font-18 mdi mdi-numeric-2-box-multiple-outline"></i> Table Without Outside Padding</h6>
            </div>
        </div>
    </div>
</div>
@endsection