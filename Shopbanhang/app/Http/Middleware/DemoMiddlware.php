<?php

namespace App\Http\Middleware;

use Closure;

class DemoMiddlware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // Middleware: kiem tra cac route co duoc quyen truy cap cac contrller hay khong
    public function handle($request, Closure $next)
    {
        if($request->is('demo/*')){
            return $next($request);
        }else{
            return redirect('/');
        }
       
    }
}
