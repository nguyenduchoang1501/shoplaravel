<?php

namespace App\Http\Controllers;

use App\Players;
use Illuminate\Http\Request;
use App\Http\Requests\PlayerRequest;
use DB;

class PlayersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = players::all()->toArray();

        // return view('frontend.list',$data);
      //query builder
        // $data = DB::table('players')->get();
        return view('frontend.list', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlayerRequest $request)
    {

        $data = new players;
        $data->name = $request->name;
        $data->date = $request->date;
        $data->salary = $request->salary;
        $data->player_position = $request->national;
        $data->player_national =$request->position;
        $data->save();
    //---------- query builder
       // $name = $request->name;
       // $date = $request->date;
       // $salary = $request->salary;
       // $national = $request->national;
       // $position = $request->position;
       //  DB::table('players')->get();
       //  $data = DB::table('players')->insert([
       //   'name' => $name,
       //   'date' => $date,
       //   'salary' => $salary,
       //   'player_national' => $national,
       //   'player_position' => $position 
       // ]);
        return redirect('/');

}
    /**
     * Display the specified resource.
     *
     * @param  \App\Players  $players
     * @return \Illuminate\Http\Response
     */
    public function show(Players $players)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Players  $players
     * @return \Illuminate\Http\Response
     */
    public function edit(PlayerRequest $request, $id)
    {
        
        $data = players::find($id);
        $data->name = $request->name;
        $data->date = $request->date;
        $data->salary = $request->salary;
        $data->player_position = $request->national;
        $data->player_national =$request->position;
        $data->save();
        $data = players::all()->toArray();
        return view('frontend.list', compact('data'));

        //query builder
        // DB::table('players')->where('id','=',$request->id)->update(['name' => $request->input('name'),
        //     'date' => $request->input('date'),
        //     'salary' => $request->input('salary'),
        //      'player_national' => $request->input('national'),
        //  'player_position' =>$request->input('position')]);

        //  $data = DB::table('players')->get();
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Players  $players
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id)
    {
        $data = players::find($id)->toArray();
        return view('frontend.update',compact('data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Players  $players
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        players::find($id)->delete();
        $data = players::all()->toArray();
        return view('frontend.list', compact('data'));
  
        // query builder
        // DB::table('players')->where('id',$id)->delete();
        // $data = DB::table('players')->get();
        // return view('frontend.list', compact('data'));
    }

    
}
