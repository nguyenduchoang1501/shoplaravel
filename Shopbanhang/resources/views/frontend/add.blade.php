
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
</head>
<body>
	@if(count($errors) >0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
     @endif
	<form action="{{route('player.create')}}" method="POST">
		<legend>Form Add</legend>

		<!-- @csrf: XAC NHAN PHUONG THUC TRUYEN VAO DANG BAO MAT -->
		@csrf
		<div class="form-group">
			<label for="">Player Name</label>
			<input type="text" class="form-control" placeholder="Player Name" name="name"><br>
			<label for="">Age</label>
			<input type="text" class="form-control" placeholder="Date" name="date"><br>
			<label for="">Salary</label>
			<input type="text" class="form-control" placeholder="Salary" name="salary"><br>
			<label for="">National</label>
			<input type="text" class="form-control" placeholder="National" name="national"><br>
			<label for="">Position</label>
			<input type="text" class="form-control" placeholder="Position" name="position"><br>
		</div>
		<button type="submit" class="btn btn-primary" name="add">Submit</button>
	</form>
</body>
</html>