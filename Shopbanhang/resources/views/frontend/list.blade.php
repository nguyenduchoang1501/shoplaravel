<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <style type="text/css">
            table{
                width: 800px;
                margin: auto;
                text-align: center;
            }
            tr {
                border: 1px solid;
            }
            th {
                border: 1px solid;
            }
            td {
                border: 1px solid;
            }
            h1{
                text-align: center;
                color: red;
            }
            #button{
                margin: 2px;
                margin-right: 10px;
                float: right;
            }
            .search{
                text-align: center;
            }
        </style>
    </head>
    <body>
    	@if(isset($success))
		<div class="alert alert-success" role="alert">{{ $success }}</div>
	    @endif
	    @if(isset($error))
			<div class="alert alert-danger" role="alert">{{ $error }}</div>
	    @endif
        <form class="search" action="" method="POST" >
            <input type="search" name="name_search">
            <input type="submit" name="ip_search">
        </form>
        <table id="datatable" style="border: 1px solid">
           		<h1>Quản lý cầu thủ:</h1>
            	<thead>
	                <tr role="row">
	                    <th>ID</th>
	                    <th>Tên cầu thủ</th>
	                    <th>Ngày sinh</th>
	                    <th>Quốc tịch</th>
	                    <th>Vị trí</th>
	                    <th>Lương</th>
	                    <th style="width: 7%;">Edit</th>
	                    <th style="width: 10%;">Delete</th>
	                </tr>
            	</thead>
        <?php if (!empty($data)) {
        	foreach ($data as $key => $value) {?>
        		<thead>
           			<tr role="row">
		            	<td>{{$value['id']}}</td>
		                <td>{{$value['name']}}</td>
		                <td>{{$value['date']}}</td>
		                <td>{{$value['player_national'] }}</td>
		               	<td>{{$value['player_position']}}</td>
		                <td>{{$value['salary']}}</td>
		                <!-- ORM eloquent -->
		                 <td style="width: 10%;"><a href="{{url('demo/update/'.$value['id'])}}">Update</a></td>
		                <td style="width: 10%;"><a href="{{url('demo/delete/'.$value['id'])}}">Delete</a></td>
		                <!-- query builder -->
	           		</tr>
        		</thead>
       		 <?php } } ?>
       		    <tfoot>
                <tr>
                    <td colspan="8">
                        <a href="{{ route('player.create') }}"><button id="button">Thêm cầu thủ</button></a>
                    </td>
                </tr>
            	</tfoot>
        </table>
    </body>
</html>