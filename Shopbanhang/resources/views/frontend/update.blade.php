
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
</head>
<body>
		@if(count($errors) >0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
     @endif
	<form action="{{route('player.create')}}" method="POST">
	<form  method="POST" role="form">
		<legend>Form Update</legend>
		<!-- @csrf: XAC NHAN PHUONG THUC TRUYEN VAO DANG BAO MAT -->
		@csrf
		<div class="form-group">
			<input type="hidden" name="id" value="{{$data['id']}}"><br>
			<label for="">Player Name</label>
			<input type="text" class="form-control" placeholder="{{$data['name']}}" name="name"><br>
			<label for="">Age</label>
			<input type="text" class="form-control" placeholder="{{$data['date']}}" name="date"><br>
			<label for="">Salary</label>
			<input type="text" class="form-control" placeholder="{{$data['salary']}}" name="salary"><br>
			<label for="">National</label>
			<input type="text" class="form-control" placeholder="{{$data['player_national']}}" name="national"><br>
			<label for="">Position</label>
			<input type="text" class="form-control" placeholder="{{$data['player_position']}}" name="`"><br>
		</div>
		<button type="submit" class="btn btn-primary" name="update">Submit</button>
	</form>
</body>
</html>