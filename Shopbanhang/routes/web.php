<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

//Route::get('/', 'HomeController@index');


// tao group de nhom cac trang co middleware
//goi middleware co ten demo tu Kernel sau khi dang ky o foder http
	Route::get('/', 'PlayersController@index')->name('player.index');
Route::middleware('demo')->group(function(){


	Route::get('demo/add', 'PlayersController@create')->name('player.create');
	Route::post('demo/add', 'PlayersController@store')->name('player.store');

	// k dung route
	Route::get('demo/delete/{id}', 'PlayersController@destroy');
	Route::get('demo/update/{id}', 'PlayersController@update');
	Route::post('demo/update/{id}', 'PlayersController@edit');
});
