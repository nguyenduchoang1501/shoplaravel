<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
     protected $table = "categories";

    public $fillable = ['name', 'slug'];

    public function posts(){
    	return hasmany(Post::class,'category_id','id');
    }
}
