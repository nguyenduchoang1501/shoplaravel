<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";
    public $fillable = ['title','content','image','view_count','user_id','new_post','slug','category_id','hightlight_post'];


    //tao quan he 
    public function user(){
    	return $this->belongsto(User::class,'user_id','id');
    }
     public function category(){
    	return $this->belongsto(Category::class,'category_id','id');
    }

     public function comments(){
    	return $this->hasmany(Comment::class,'post_id','id');
    }

}
