<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
     protected $table = "comments";

    public $fillable = ['content', 'user_id','post_id'];

     public function user(){
    	return $this->belongsto(User::class,'user_id','id');
    }
 	public function post(){
    	return $this->belongsto(Post::class,'post_id','id');
    }}
